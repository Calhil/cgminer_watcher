rig0 = {'host': '192.168.0.100', 'port': 12348}
rig1 = {'host': '192.168.0.101', 'port': 12347}
rig_blake = {'host': '192.168.0.101', 'port': 12348}

# list all rigs to monitor
rigs = [rig1]


# logging
# =======

# log filename
LOG_FILENAME = '/home/bartosz/Applications/cgminer_watcher/cgminer.log'


# control variables
# =================
MONITORING_DELAY = 30  # [s]
