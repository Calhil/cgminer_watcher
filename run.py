from config import rigs
from config import LOG_FILENAME
from config import MONITORING_DELAY

import pycgminer
import logging
from time import sleep
from time import time
from os import system


# logging
logging.getLogger('cgminer_watcher').handlers = []
logger = logging.getLogger('cgminer_watcher')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s]:%(levelname)s:%(message)s')

filehandle = logging.FileHandler(LOG_FILENAME)
filehandle.setLevel(logging.WARNING)
filehandle.setFormatter(formatter)
logger.addHandler(filehandle)

consolehandle = logging.StreamHandler()
consolehandle.setLevel(logging.DEBUG)
consolehandle.setFormatter(formatter)
logger.addHandler(consolehandle)


while True:
    # check the rigs
    for i_rig in rigs:
        try:
            rig_api = pycgminer.CgminerAPI(**i_rig)
            for i_gpu in rig_api.devs()['DEVS']:
                logger.info('{0}:{1} <{2}>, temp = {3}'.format(i_rig['host'], i_gpu['GPU'], i_gpu['Status'], i_gpu['Temperature']))  # Alive, Dead, Sick

                if i_gpu['Status'] == 'Dead':
                    logger.warning('{0}: GPU{1} is {2} at {3}'.format(
                        i_rig['host'], i_gpu['GPU'], i_gpu['Status'], time()))
                    # restart the sick/dead rigs
                    # assumes all rigs are run on localhost
                    logger.warning('Rebooting the system.')
                    system("/usr/bin/sudo /sbin/reboot")

        except Exception as e:
            logger.warning('Caught exception {1}: <{0}>'.format(
                e,
                type(e).__name__))
            #logger.warning('Rebooting the system.')
            #system("/usr/bin/sudo /sbin/reboot")




    sleep(MONITORING_DELAY)
